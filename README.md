# 😘vue-form-validator

[![npm](https://img.shields.io/npm/v/@maybecode/vue-form-validator.svg)](https://www.npmjs.com/package/@maybecode/vue-form-validator)
[![npm](https://img.shields.io/npm/dt/@maybecode/vue-form-validator.svg)](https://www.npmjs.com/package/@maybecode/vue-form-validator)
[![npm](https://img.shields.io/bundlephobia/min/@maybecode/vue-form-validator.svg)](https://www.npmjs.com/package/@maybecode/vue-form-validator)

> <p>🌟 基于async-validator封装的高度定制化UI的表单验证组件，不提供任何UI封装，只封装验证逻辑</p>

## Demo

<a href="http://null_639_5368.gitee.io/vue-form-validator">demo</a>

## 安装

```
npm install @maybecode/vue-form-validator

或者

yarn add @maybecode/vue-form-validator
```

```
// main.js

import VueFormValidator from "@maybecode/vue-form-validator";

Vue.use(VueFormValidator);
```

## mv-form props

---

<code>
<table>
<tr>
<td>名称</td><td>类型</td><td>描述</td>
</tr>
<tr>
<td>model</td>
<td>object</td>
<td>验证的表单对象</td>
</tr>
<tr>
<td>rules</td>
<td>object</td>
<td>验证规则对象</td>
</tr>
<tr>
<td>custom-class</td>
<td>string</td>
<td>自定义类名</td>
</tr>
</table>
</code>

## mv-item props

---

<code>
<table>
<tr>
<td>名称</td><td>类型</td><td>描述</td>
</tr>
<tr>
<td>prop</td>
<td>string</td>
<td>验证的表单字段</td>
</tr>
<tr>
<td>rules</td>
<td>[]</td>
<td>自定义字段的验证规则</td>
</tr>
<tr>
<td>custom-class</td>
<td>string</td>
<td>自定义类名</td>
</tr>
</table>
</code>

## mv-item scope-slots

---

<table>
<tr>
<td>名称</td><td>类型</td><td>描述</td>
</tr>
<tr>
<td>errors</td>
<td>[]</td>
<td>返回的数组信息数据</td>
</tr>
<tr>
<td>fieldValidate</td>
<td>function</td>
<td>验证当前字段的函数</td>
</tr>
</table>

```
<template #default="{errors,fieldValidate}">
<!-- 这里插入内容 -->
</template>
```

```
      <template #default="{errors,fieldValidate}">
          <div class="item-row">
            <input type="text" @blur="fieldValidate" v-model="form.code" />
          </div>
          <p v-if="errors && errors.length>0">{{errors}}</p>
        </template>
```

## 👉 模板

---

```
 <div class="home">
    <img alt="Vue logo" src="../assets/logo.png" />
    <h2>😘vue-form-validator</h2>
    <p>🌟可以高度定制化UI的表单验证组件，不提供任何UI封装，只封装验证逻辑</p>
    <mv-form :model="form" :ref="el" :rules="rules" custom-class="custom-form">
      <h2>默认表单提交触发验证👇</h2>
      <mv-item prop="phone" custom-class="custom-item">
        <template #default="{errors}">
          <div class="item-row">
            <label>手机号:</label>
            <input v-model="form.phone" type="text" />
          </div>
          <p v-if="errors && errors.length>0">{{errors}}</p>
        </template>
      </mv-item>
      <h2>输入触发事件👇</h2>
      <mv-item prop="password" custom-class="custom-item">
        <template #default="{errors,fieldValidate}">
          <div class="item-row">
            <label>密码:</label>
            <!-- 触发函数绑定到自定义的事件上 -->
            <input type="password" @input="fieldValidate" v-model="form.password" />
          </div>
          <p v-if="errors && errors.length>0">{{errors}}</p>
        </template>
      </mv-item>
      <h2>失去焦点触发事件👇</h2>
      <mv-item prop="code" custom-class="custom-item">
        <template #default="{errors,fieldValidate}">
          <div class="item-row">
            <label>验证码:</label>
            <input type="text" @blur="fieldValidate" v-model="form.code" />
          </div>
          <p v-if="errors && errors.length>0">{{errors}}</p>
        </template>
      </mv-item>
      <h2>select值改变触发事件👇</h2>
      <mv-item prop="select" :rules="[{ required: true, message:'必须选择一个选项',type:'string'}]">
        <template #default="{errors,fieldValidate}">
          <div class="item-row">
            <label>选项验证:</label>
            {{form.select}}
            <select
              v-model="form.select"
              @change="customSelectChange(fieldValidate)"
            >
              <option :value="null">空值</option>
              <option value="1">第一个</option>
              <option value="2">第二个</option>
            </select>
          </div>
          <p v-if="errors && errors.length>0">{{errors}}</p>
        </template>
      </mv-item>
      <!-- 这里要设置button的type为submit-->
      <button style="margin:15px 0px;" type="submit" @click.prevent="validate">提交</button>
    </mv-form>
  </div>
```

## 👉 配置

---

```
export default {
  name: "Home",
  components: {
  },
  data() {
    return {
      el: "formbbb",
      form: {
        username: "",
        password: "",
        code: "",
        select: "",
      },
      // 具体验证规则参考 https://github.com/yiminghe/async-validator
      rules: {
        phone: [
          { required: true, message: "请填写手机号" },
          {
            message: "请输入正确手机号",
            // 自定义验证逻辑
            validator(rule, value, callback) {
              const reg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
              return reg.test(value);
            },
          },
        ],
        password: [
          { required: true, message: "请填写密码", type: "string" },
          { message: "密码最多10位", max: 10 },
        ],
        code: [
          { required: true, message: "请填写验证码", type: "string" },
          { message: "验证码4位", len: 4 },
        ]
      },
    };
  },
  methods: {
    customSelectChange(validate) {
      // 自定义验证的时机
      validate();
    },
    // 表单提交验证
    validate() {
      console.log(this.$refs);
      this.$refs[this.el].validate((err) => {
        console.log(err);
      });
    },
  },
};

```

### Typescript

```
import { MvForm } from "@maybecode/vue-form-validator";
import { Rules } from 'async-validator'

export default {
  name: "Home",
  components: {
  },
  data() {
    return {
      el: "formbbb",
      form: {
        username: "",
        password: "",
        code: "",
        select: "",
      },
      // 具体验证规则参考 https://github.com/yiminghe/async-validator
      rules: {
        phone: [
          { required: true, message: "请填写手机号" },
          {
            message: "请输入正确手机号",
            // 自定义验证逻辑
            validator(rule, value, callback) {
              const reg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
              return reg.test(value);
            },
          },
        ],
        password: [
          { required: true, message: "请填写密码", type: "string" },
          { message: "密码最多10位", max: 10 },
        ],
        code: [
          { required: true, message: "请填写验证码", type: "string" },
          { message: "验证码4位", len: 4 },
        ]
      } as Rules,
    };
  },
  methods: {
    customSelectChange(validate) {
      // 自定义验证的时机
      validate();
    },
    // 表单提交验证
    validate() {
      console.log(this.$refs);
      ( this.$refs[this.el] as MvForm).validate((err) => {
        console.log(err);
      });
    },
  },
};
```
