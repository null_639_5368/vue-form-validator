import Vue from "vue";

export const install: () => {};

export type Errors = [];
export class MvForm extends Vue {
  validate: (fn: (errs: Errors) => void) => Promise<Errors>;
}
export class MvItem extends Vue {}

export type FieldValidate = () => Promise<Errors>;
