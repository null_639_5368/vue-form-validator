/* eslint-disable */

const path = require("path");

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  publicPath: "/vue-form-validator",
  outputDir: process.env.IS_LIB ? "lib-dist" : "dist",
  chainWebpack(config) {
    // 设置别名
    config.resolve.alias.set("package", resolve("package"));
  },
};
