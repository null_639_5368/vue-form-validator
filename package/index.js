import MvForm from './Form/Form.vue';
import MvItem from './Item/Item.vue';

const componentsName = ['MvForm', 'MvItem'];

const components = [MvForm, MvItem];

const install = function (Vue) {
  components.map((component, i) => {
    Vue.component(componentsName[i], component);
  });
};

export default { install, MvForm, MvItem };
