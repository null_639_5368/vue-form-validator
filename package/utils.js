export const isHasOwnProperty = (obj, key) => {
  return Object.prototype.hasOwnProperty.call(obj, key);
};
