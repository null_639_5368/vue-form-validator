module.exports = {
  semi: false, // 不使用分号
  singleQuote: true, // 使用单引号
  trailingComma: false // 对象最后一项默认格式化会加逗号
}
